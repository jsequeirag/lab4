###############################################EJERCICIO 4  Y 5##################################################################
# se instala la libreria para conectar con la base de oracle
install.packages("RODBC")#----->ESTA
install.packages("ggplot2")#------>PARA GRAFICOS
install.packages("dplyr")#-------->OTRAS FUNCIONES
#

#se llaman las librerias
library(RODBC)
library(ggplot2)
library(dplyr,warn.conflicts = FALSE)
#

#------------------>EJERCICIO 5<-----------------------------------#

#se cargan las variables de entorno
SOURCE <- Sys.getenv("SOURCE")

USER <- Sys.getenv("USER")

PWD <- Sys.getenv("PWD")
#

#------------------>EJERCICIO 4<-----------------------------------#

# se ejecuta el siguiente codigo para crear la conexión
instance1<-odbcConnect(SOURCE, USER ,  PWD)
summary(instance1)
#

###############################################EJERCICIO 6##################################################################

#CARGO LAS VISTA DE LAS TABLAS:CLIENTE,PEDIDO Y DETALLE_PEDIDO
CLIENTE<-sqlQuery(instance1,"select *  from CLIENTE")
PEDIDO<-sqlQuery(instance1,"select *  from PEDIDO")
DETALLE_PEDIDO<-sqlQuery(instance1,"select *  from DETALLE_PEDIDO")
#

# crear las vistas
View(dfjoin1)
View(PEDIDO)
View(DETALLE_PEDIDO)
#

#analizar los datos de las vistas
glimpse(CLIENTE)
glimpse(PEDIDO)
glimpse(DETALLE_PEDIDO)
#

###############################################EJERCICIO 7##################################################################

# (dfjoin1)UNO PRIMERO LAS TABLAS cliente-pedido por codigo cliente
# (dfjoin2)despues uno el dfjoin1 con la tabla detalle_pedido por codigo_pedido

dfjoin1=merge(x=CLIENTE,y=PEDIDO , by="CODIGO_CLIENTE")
view(dfjoin1)
dfjoin2=merge(x=dfjoin1,y=DETALLE_PEDIDO,by="CODIGO_PEDIDO")
#

###############################################EJERCICIO 8##################################################################

#exploración de datos
str(dfjoin2)
head(dfjoin2)
tail(dfjoin2)
class(dfjoin2)
summary(dfjoin2)
which(is.na(dfjoin2))
names(dfjoin2)
colnames(dfjoin2)
#exploración de datos
dfjoin2
attach(dfjoin2) 
#

###############################################EJERCICIO 9##################################################################

#se convierte de texto a factor
NNOMBRE_CLIENTE=as.factor(NOMBRE_CLIENTE)
NCIUDAD=as.factor(CIUDAD)
NREGION=as.factor(REGION)
NPAIS=as.factor(PAIS)
NESTADO=as.factor(ESTADO)
NCANTIDAD=as.factor(CANTIDAD)
NCODIGO_PRODUCTO=as.factor(CODIGO_PRODUCTO)
NNOMBRE_PRODUCTO=as.factor(NOM )

#Se convierte de character a date
NFECHA_PEDIDO=as.Date(FECHA_PEDIDO)

#Se convierte de character a double
NPRECIO_UNIDAD=as.double(PRECIO_UNIDAD)
NCANTIDAD=as.double(CANTIDAD)

###############################################EJERCICIO 10##################################################################

table(dfjoin2$NOMBRE_CLIENTE,exclude = NULL)

###############################################EJERCICIO 11 , 12  Y 13##################################################################

#SE CREA LA NUEVA TABLA 
#MES=format(NFECHA_PEDIDO,'%m')
#total_ventas=NCANTIDAD * NPRECIO_UNIDAD

newTable=data.frame(NNOMBRE_CLIENTE,NCIUDAD,NREGION,
                    NPAIS,NFECHA_PEDIDO,MES=format(NFECHA_PEDIDO,'%m'),
                    AÑO=format(NFECHA_PEDIDO,'%Y'),#----->EJERCICIO13
                    NESTADO,NCANTIDAD,NPRECIO_UNIDAD,
                    NCODIGO_CLIENTE,NCODIGO_PRODUCTO,
                    total_ventas=NCANTIDAD * NPRECIO_UNIDAD)#----->EJERCICIO12

###############################################EJERCICIO 14##################################################################

#SABER SI HAY VALORES NULL
table(newTable$NCIUDAD ,useNA='always')
table(newTable$NREGION   ,useNA='always')
is.na(newTable) 
apply(is.na(newTable), 2, sum)
###############################################EJERCICIO 15##################################################################
data<-as.integer(newTable$total_ventas)
boxplot(newTable$total_ventas, main="Total de ventas",horizontal = T,boxwex=1.5)$out
stripchart(data,method = "jitter",pch=19,add = T,col = "blue")

###############################################EJERCICIO 16##################################################################

hist(data)
hist(newTable$total_ventas,col = 'lightblue',main = "Histograma de las iris")
invoices1997<-invoices %>% filter(year==1997)

# histogram with added parameters





ventas_ciudad<-data.frame (total_ventas=newTable$NCANTIDAD * newTable$NPRECIO_UNIDAD,NCIUDAD)

ventas_ciudad2<-ventas_ciudad%>% group_by(NCIUDAD)


ventas_anuales<- data.frame (total_ventas=newTable$NCANTIDAD * newTable$NPRECIO_UNIDAD,AÑO=format(NFECHA_PEDIDO,'%Y'))

ventas_anualesf <-ventas_anuales%>%filter(AÑO!=2006)

ventas_anualesg <-ventas_anualesf%>%group_by(AÑO)  

ventas_resumen<-data.frame(total_ventas=newTable$NCANTIDAD * newTable$NPRECIO_UNIDAD,NNOMBRE_CLIENTE,NPAIS)
ventas_resumenG<-ventas_resumen%>% group_by(NPAIS)%>%summarise(total_clientes = n(),ventas_totales=sum(total_ventas))


ventas_resumen2<-data.frame(total_ventas=newTable$NCANTIDAD * newTable$NPRECIO_UNIDAD,NNOMBRE_CLIENTE,NPAIS,NCODIGO_PRODUCTO)
ventas_resumenG2<-ventas_resumen2%>% group_by(NPAIS,NCODIGO_PRODUCTO)%>%summarise(total_clientes = n(),ventas_totales=sum(total_ventas))

###############################################EJERCICIO 17##################################################################

ventas_ciudad=newTable%>%group_by(NCIUDAD)

###############################################EJERCICIO 18##################################################################

ventas_anuales=newTable%>%group_by(AÑO)%>%filter(AÑO!=2006)

###############################################EJERCICIO 19##################################################################

ventas_resumen1<-newTable %>% group_by(NPAIS)%>%summarise(total_clientes = n(),ventas_totales=sum(total_ventas))


###############################################EJERCICIO 20##################################################################

ventas_resumen2<-newTable%>% group_by(NPAIS,NCODIGO_PRODUCTO)%>%summarise(total_clientes = n(),ventas_totales=sum(total_ventas))
                                       
###############################################EJERCICIO 21-GRAFICOS##################################################################  

#######GRAFICOS-ventas_ciudad

ggplot(data =ventas_ciudad2,aes(x=NCIUDAD ,y=prueba1$total_ventas, NCIUDAD) )+
  geom_bar(stat = "identity",col="red")+
  geom_text(aes(label=prueba1$total_ventas,hjust=0.5, size=5 , fontsize="bold"))+
  scale_y_continuous(labels = scales::comma)+
  theme(legend.position = "none")+
  coord_flip()

ggplot(data = ventas_ciudad)+
  aes(x=total_ventas,y=..density..)+
  geom_histogram(bins = 15,color="Blue",fill="lightblue")+
  geom_density(size=2,color="blue")+
  labs(title = "Histograma y densidad ",subtitle = "Datos de Totales",
       caption = "datos por años")+
  theme(plot.title = element_text(color="Red", size = 16 ,hjust = 0),
        plot.subtitle =  element_text(color="Red", size = 10 ,hjust = 2),
        plot.caption = element_text(color="green", size = 16 ,hjust = 1))

#######GRAFICOS-ventas_anuales

ggplot(data =ventas_anuales,aes(x=AÑO  ,y=total_ventas, AÑO) )+
  geom_bar(stat = "identity",col="red")+
  geom_text(aes(label=total_ventas,hjust=0.5, size=5 , fontsize="Bold"))+
  scale_y_continuous(labels = scales::comma)+
  theme(legend.position = "none")+
  coord_flip()

ggplot(data = ventas_anuales)+
  aes(x=total_ventas,y=..density..)+
  geom_histogram(bins = 15,color="Blue",fill="lightblue")+
  geom_density(size=2,color="blue")+
  labs(title = "Histograma y densidad ",subtitle = "Datos de Totales",
       caption = "datos por años")+
  theme(plot.title = element_text(color="Red", size = 16 ,hjust = 0),
        plot.subtitle =  element_text(color="Red", size = 10 ,hjust = 2),
        plot.caption = element_text(color="green", size = 16 ,hjust = 1))

######GRAFICOS-resumen1

ggplot(data =ventas_resumen1,aes(x=NPAIS  ,y=ventas_totales, NPAIS) )+
  geom_bar(stat = "identity",col="red")+
  geom_text(aes(label=ventas_totales,hjust=0.5, size=5 , fontsize="Bold"))+
  scale_y_continuous(labels = scales::comma)+
  theme(legend.position = "none")+
  coord_flip()

ggplot(data = ventas_resumen1)+
  aes(x=ventas_totales,y=..density..)+
  geom_histogram(bins = 15,color="Blue",fill="lightblue")+
  geom_density(size=2,color="blue")+
  labs(title = "Histograma y densidad ",subtitle = "Datos de Totales",
       caption = "datos por años")+
  theme(plot.title = element_text(color="Red", size = 16 ,hjust = 0),
        plot.subtitle =  element_text(color="Red", size = 10 ,hjust = 2),
        plot.caption = element_text(color="green", size = 16 ,hjust = 1))

######GRAFICOS-resumen2

ggplot(ventas_resumen2,aes(x=NPAIS  ,y=ventas_totales, NPAIS) )+
  geom_bar(stat = "identity",col="red")+
  geom_text(aes(label=ventas_totales,hjust=0.5, size=5 , fontsize="Bold"))+
  scale_y_continuous(labels = scales::comma)+
  theme(legend.position = "none")+
  coord_flip()


ggplot(data = ventas_resumen2)+
  aes(x=ventas_totales,y=..density..)+
  geom_histogram(bins = 15,color="Blue",fill="lightblue")+
  geom_density(size=2,color="blue")+
  labs(title = "Histograma y densidad ",subtitle = "Datos de Totales",
       caption = "datos por años")+
  theme(plot.title = element_text(color="Red", size = 16 ,hjust = 0),
        plot.subtitle =  element_text(color="Red", size = 10 ,hjust = 2),
        plot.caption = element_text(color="green", size = 16 ,hjust = 1))

